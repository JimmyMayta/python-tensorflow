## OpenCV

### Programas

* Anaconda: <https://www.anaconda.com/>


### Librerías
* OpenCV: <https://opencv.org/>
* TensorFlow: <https://www.tensorflow.org/>
* PIP:<https://pypi.org/project/pip/>

### Instalación usando pip (Linux y Python 3.x.x)
* pip install opencv-python
* pip install opencv-contrib-python
* pip install tensorflow (Recomendado)
* pip install numpy
* pip install scikit-learn
* pip install matplotlib

### TensorFLow
- Numpy
- Clasificador Scikitlearn
- Clasificación usando regresión logística
- Construyendo un modelo usando perceptrón
- Clasificación construyendo un perceptrón
- Clasificación usando Keras
- Análisis de MNIST


